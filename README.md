# Obfuscated Text classification using LSTM
The text used is obfuscated/deformed.

There are 12 classes (books) in my data. Classes/Labels can be book/genre/author/mood or anything that can be inferenced by using text.

There are enough comments in the code to explain stuff.

# Obfuscated text example
## satwamuluhqgulamlrmvezuhqvkrpmletwulcitwskuhlemvtwamuluhiwiwenuhlrvimvqvkruhulenamuluhqgqvtwvimviwuhtwamuluhulqvkrenamcitwuhvipm

Yes this is how the text looks like. There are **no spaces**, so **no words** and task is to tell the book which this text belongs to.
Every alphabet is considered as a unit (in general every word is treated as a unit).



## Requirments 
Tensorflow : [Instructions to install tensorflow](https://www.tensorflow.org/install/) 

simply try to install packages using pip:
```shell
$ pip install keras    # a wrapper over tensorflow
$ pip install matplotlib      # to plot graphs
$ pip install numpy
$ pip install os
```
## Input data
Text file in which each line represents a sample x_train[i].

## Code Description
There are two files **train.py** and **predict.py**
Everything is explained in the code.

It has 6 major sections:

Section 1: loading text from txt files. In my case the file is named as xtrain_obfuscated.txt

Section 2: Setting of parameters to play with.

Section 3: Building the model

Section 4: Training the model

Section 5: Saving the best model.

Section 6: Plotting the train/test accuracy.

## How to run
open terminal at the location where **train.py** file is located.

```shell
$ python train.py 
```

## Results
![ACCURACY](graphs/accuracy.png)
![LOSS](graphs/loss.png)

You can find the code to plot these graphs at the end of **train.py** file.

## Applications

Learning features from text which makes no sense to human. Completely deformed/obfuscated/jumbled up text.

* email classification. (spam/non-spam )
* tweets classification. (pro-modi/pro-kejriwal)
* google news classifier which splits news into sports/politics/tech/entertainment etc.
* text style classifier according to author.
* using tweets to detect depression.
* many more..

## Lots of comments in the code to explain stuff

e-mail me incase you also need the training and testing data.
nikhil.chhabra[at]rwth-aachen[dot]de
